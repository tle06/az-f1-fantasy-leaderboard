import logging
import json
import azure.functions as func
from src.util.functions import build_response


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    data = json.dumps(build_response())
    return func.HttpResponse(data)
