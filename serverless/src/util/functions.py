
from src.model.f1 import F1Scores, Users
from src.util.db import db_init_session
import logging
import json
from sqlalchemy.orm import class_mapper

def asdict(obj):
    return dict((col.name, getattr(obj, col.name))
                for col in class_mapper(obj.__class__).mapped_table.c)

def get_usernames():
    session = db_init_session()
    data: list = []
    for row in session.query(Users).all():
        #logging.info("data = %s",asdict(row))
        data.append(asdict(row))
    session.close()

    return data

def get_data_per_username(username:str):
    session = db_init_session()
    data: list = []
    for row in session.query(F1Scores).filter_by(username=username).all():
        #logging.info("data = %s",row)
        data.append(asdict(row))
    session.close()

    return data

def build_response():

    usernames:list = get_usernames()
    response:list = []

    for user in usernames:
        username:str = user['username']
        user_id:str = user['id']
        logging.info("username = %s",username)
        logging.info("user_id = %s",user_id)
        data = get_data_per_username(username=username)
        #logging.info("data = %s",data)
        total_score: float = 0.0

        for row in data:
             total_score = total_score + row['team_score']
        
        logging.info("total_score = %s",total_score)

        object_res = {
            "id": user_id,
            "username": username,
            "total_score": total_score,
            "teams": data
        }

        response.append(object_res)

        #logging.info("data = %s",object_res)
    return response
