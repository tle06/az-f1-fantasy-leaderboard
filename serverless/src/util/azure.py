import os
import cmd
from azure.keyvault.secrets import SecretClient
from azure.identity import DefaultAzureCredential
import json

def get_secret(vault_name:str,secret_name:str):
    KVUri = f"https://{vault_name}.vault.azure.net"
    credential = DefaultAzureCredential()
    az_client = SecretClient(vault_url=KVUri, credential=credential)
    retrieved_secret = json.loads(az_client.get_secret(secret_name).value)
    
    return retrieved_secret