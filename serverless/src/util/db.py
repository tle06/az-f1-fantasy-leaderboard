from src.model.f1 import db_connect
from sqlalchemy.orm import Session


def db_init_session():
    engine = db_connect()
    session = Session(engine)
    return session

def save_data(data):
    session = db_init_session()
    try:
        session.add(data)
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()

    return data