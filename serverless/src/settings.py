import os
from pathlib import Path

SQLALCHEMY_DATABASE_URI = "{drivername}://{user}:{passwd}@{host}:{port}/{db_name}?charset=utf8".format(
    drivername= os.getenv("DB_DRIVER") or "mysql+mysqlconnector",
    user=os.environ["DB_USER"] or "admin",
    passwd=os.environ["DB_PASSWORD"] or "admin",
    host=os.environ["DB_HOST"] or "localhost",
    port=os.environ["DB_PORT"] or "3306",
    db_name=os.environ["DB_NAME"] or "tf_f1-fantasy",
)

        