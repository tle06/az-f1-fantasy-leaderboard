
from sqlalchemy import inspect
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine, Column, ForeignKey
import src.settings
from sqlalchemy import (Integer, String, DateTime, Float,  Text, Boolean)

Base = declarative_base()

def db_connect():
    """
    Performs database connection using database settings from settings.py.
    Returns sqlalchemy engine instance
    """
    return create_engine(src.settings.SQLALCHEMY_DATABASE_URI)

class F1Scores(Base):

    __tablename__ = 'f1_scores'

    id = Column(Integer, autoincrement=True, unique=True, primary_key=True, nullable=False,index=True)
    user_id = Column(Integer,nullable=False)
    username = Column(Text,nullable=True)
    gp_name = Column(Text,nullable=True)
    team_name = Column(Text,nullable=True)
    team_score = Column(Float,nullable=True)
    

class Users(Base):

    __tablename__ = 'f1_users'

    id = Column(Integer, autoincrement=True, unique=True, primary_key=True, nullable=False,index=True)
    username = Column(Text,nullable=False)
